import json
from typing import List

import typer
from server import ServerService, CreateOrUpdateMessageSchema, CreateOrUpdatePersonnelSchema, \
    CreateOrUpdateClientSchema, SelectPersonnelSchema, SelectClientSchema

app = typer.Typer()


@app.command()
def define_client(name: str, host: str = "127.0.0.1", port: int = 8000):
    """
    Create client in server DB

    name: required client name

    --host: optional client host
    --port: optional client port
    """
    service = ServerService()
    client_model = CreateOrUpdateClientSchema(name=name, host=host, port=port)
    service.add_client(client_model)


@app.command()
def define_personnel(name: str, surname: str, ssn: str):
    """
    Create personnel in server DB

    name: required personnel name
    surname: required personnel surname
    ssn: required personnel ssn
    """
    try:
        service = ServerService()
        personnel_model = CreateOrUpdatePersonnelSchema(name=name, surname=surname, ssn=ssn)
        service.add_personnel(personnel_model)
    except Exception as e:
        print(e)


@app.command()
def define_message(message_type: str, client_id: int, personnel_id: int = None) -> None:
    """
    Create message in server DB
    message_type: required message type
    client_id: required if send message all request it must be -1
    --personnel_id: optional
    """
    try:
        if not (message_type == "add" or message_type == "delete" or
                message_type == "delete_all" or message_type == "add_all"):

            print("Invalid message type")
        service = ServerService()
        if client_id != -1 or service.select_client(client_id) is None:
            print("Invalid client id")

        personnel_data: dict | List[dict] | List[SelectPersonnelSchema] | None = None
        if personnel_id is not None:
            personnel_data = service.select_personnel(personnel_id).model_dump()
        else:
            personnel_data = [personnel.model_dump() for personnel in service.select_personals()]
        if personnel_data is None:
            print("Personnel not found")
        if message_type == "add":
            payload = {"message_type": message_type, "data": personnel_data}
        elif message_type == "delete":
            payload = {"message_type": message_type, "data": {"id": personnel_id}}
        elif message_type == "add_all":
            payload = {"message_type": message_type, "data": personnel_data}
        else:
            payload = {"message_type": message_type}

        client_id = client_id if client_id is not None else -1
        service.add_message(CreateOrUpdateMessageSchema(client_id=client_id, payload=payload))
    except Exception as e:
        print(e)


if __name__ == "__main__":
    app()
