import json
from time import sleep
from typing import List

from client import InovatSocketClient, ClientService
from server import InovatSocketServer, ServerService


class Operation:
    """
    This class is responsible for operational task such as
    run socket server, connect client to server, and send messages
    between peer.
    run function start task loop and run commands.
    """

    def __init__(self):
        self._server: InovatSocketServer | None = None
        self._clients: list[InovatSocketClient] = []

    def create_and_run_client(self, name: str, host: str, port: int) -> None:
        """
        Create new client in DB and run it.
        :param name: client name
        :param host: client ip
        :param port: client port
        """
        try:
            if not self._server:
                raise OperationException(f"There is no server!",
                                         self.create_and_run_client.__name__)

            self._server.create_client(name=name, host=host, port=port)
            client_service = ClientService(db_name=name)
            client = InovatSocketClient(client_service, name=name, host=host, port=port)
            client.bind(None)
            client.connect((self._server.getsockname()[0], self._server.getsockname()[1]))
            self._clients.append(client)

        except Exception as e:
            raise OperationException(e, self.create_and_run_client.__name__)

    def run_client(self, client_id: int) -> None:
        """
        Run client from DB
        :param client_id: client id
        """
        try:
            if not self._server:
                raise OperationException(f"There is no server!",
                                         self.run_client.__name__)
            client = self._server.get_client_info(client_id).model_dump()
            if client is None:
                raise OperationException(f"Client not found: {client_id}!", self.run_client)
            client_service = ClientService(db_name=client['name'])
            client = InovatSocketClient(client_service, name=client['name'], host=client['host'], port=client['port'])
            client.bind(None)
            client.connect((self._server.getsockname()[0], self._server.getsockname()[1]))
            self._clients.append(client)
        except Exception as e:
            raise OperationException(e, self.run_client.__name__)

    def run_server(self, server_host: str, server_port: int) -> None:
        """
        Run server specified by server host and port
        :param server_host: server ip
        :param server_port: server port
        """
        try:
            if self._server:
                raise OperationException(
                    f"Server already running {self._server.getsockname()[0]}:{self._server.getsockname()[1]}!",
                    self.run_server.__name__)
            server_service = ServerService()
            server = InovatSocketServer(server_service, host=server_host, port=server_port)
            server.run_server()
            self._server = server
        except Exception as e:
            raise OperationException(e, self.run_server.__name__)

    def send_message(self, message_id: int) -> None:
        """
        Send message from server to client
        """
        if not self._server:
            raise OperationException(f"There is no server!",
                                     self.run_client.__name__)
        try:
            self._server.send_message(message_id)
        except Exception as e:
            raise OperationException(e, self.send_message.__name__)

    def run(self):
        """
        Get input from user and run specified operation
        """
        run_flag = True
        while run_flag:
            print("""
Select one of the following options:
1) Run socket server
2) Create and Run socket client
3) Run saved Client
4) Send message
5) Exit
                """)
            input_choice = input("Enter your choice: ")
            if input_choice == "1":
                try:
                    host: str = input("Enter server host(Default 127.0.0.1) >")
                    port: str | int = input("Enter server port(3000) >")
                    host = host if host else "127.0.0.1"
                    port = int(port) if port else 3000
                    self.run_server(host, port)
                except KeyboardInterrupt:
                    self._server.close() if self._server is not None else print("Server closed.")
                except OperationException as e:
                    print(e)

            if input_choice == '2':
                try:
                    if not self._server:
                        raise OperationException("There is not a server", self.run.__name__)
                    client_name: str = input("Enter client name >")
                    client_host: str = input("Enter client host(Default 127.0.0.1) >")
                    client_port: int | str = input("Enter client port(Default 8000) >")
                    client_host = client_host if client_host is not None else "127.0.0.1"
                    client_port = int(client_port) if client_port is not None else 8000
                    self.create_and_run_client(client_name, client_host, client_port)
                except KeyboardInterrupt:
                    self._server.close() if self._server is not None else print("Server closed.")
                except OperationException as e:
                    print(e)

            if input_choice == '3':
                try:
                    client_list: List[dict] = self._server.get_clients_info()
                    input_text = "\n".join([f"{client['id']}) {client['name']}" for client in client_list])
                    client_id = int(input(input_text + ' >'))
                    self.run_client(client_id)
                except KeyboardInterrupt:
                    self._server.close() if self._server is not None else print("Server closed.")
                except OperationException as e:
                    print(e)

            if input_choice == '4':
                messages: List[dict] = self._server.get_messages_info()
                input_text = "\n".join([f"{message['id']}){json.dumps(message)}" for message in messages])
                input_text += ' \nSelect message above >'
                message_id = int(input(input_text))
                self.send_message(message_id)

            if input_choice == '5':
                self._server.close() if self._server is not None else print("")
                print("Terminated....")
                run_flag = False
            sleep(1)


class OperationException(Exception):
    def __init__(self, message, function):
        super().__init__(f"Error on {function}\n{message}")