import json
import socket
import threading
from time import sleep
from typing import List

from server import CreateOrUpdateClientSchema, SelectClientSchema
from server.service import ServerService


class InovatSocketServer(socket.socket):
    """
    A socket server that send data to client
    """
    def __init__(self, service: ServerService, family=socket.AF_INET,
                 type=socket.SOCK_STREAM, proto=0, fileno=None, **kwargs) -> None:
        super().__init__(family, type, proto, fileno)
        self._service = service
        self._port = kwargs.get('port') if 'port' in kwargs else 3000
        self._host = kwargs.get('host') if 'host' in kwargs else "127.0.0.1"
        self._clients = []

    def listen(self, backlog=None):
        """
        Listen for incoming connections
        :param backlog: How many times to attempt to receive
        """
        try:
            self.bind((self._host, self._port))
            super().listen(backlog) if backlog else super().listen()
            print(f'Server listening on {self._host} : {self._port}')
        except Exception as e:
            self.close()
            raise ServerException(e, self.listen.__name__)

    def accept(self):
        """
        Accept incoming connection to client
        """
        try:
            client_socket, client_addr = super().accept()
            self._clients.append(client_socket)
            return client_socket, client_addr
        except Exception as e:
            self.close()
            raise ServerException(e, self.accept.__name__)

    def close(self):
        """
        Close the socket server
        """
        print('Server closed')
        super().close()

    def run_server(self):
        """
        Run the server from new thread
        """
        self.listen()
        worker = threading.Thread(target=self.run_server_worker)
        worker.start()

    def run_server_worker(self):
        """
        Run the server and handle incoming connections
        """
        try:
            while True:
                client_socket, client_addr = self.accept()
                print(f"Accepted connection from {client_addr[0]}:{client_addr[1]}")
                thread = threading.Thread(target=self.handle_connection, args=(client_socket, client_addr))
                thread.start()
        except Exception as e:
            raise ServerException(e, self.run_server_worker.__name__)
        finally:
            self.close()

    def handle_connection(self, client_socket, client_addr):
        """
        Handle incoming connection from client
        """
        try:
            print(f"Connection from {client_addr[0]}:{client_addr[1]}")
            check_client_flag = True
            if not self.check_client_exists(client_addr):
                close_message = json.dumps({"message": "close"})
                client_socket.send(close_message.encode('utf-8'))
                check_client_flag = False
            while check_client_flag:
                response = client_socket.recv(1024)
                response = response.decode("utf-8")
                response = json.loads(response)
                print(f"Received: {response}")
                if response.message.lower() == "close":
                    close_message = json.dumps({"message": "close"})
                    client_socket.send(close_message.encode('utf-8'))
                    break
                sleep(1)
        except Exception as e:
            self.close()
            raise ServerException(e, self.handle_connection.__name__)
        finally:
            self.close()

    def get_client_with_addr(self, addr: tuple) -> socket.socket | None:
        """
        Get a client with ip and port from _clients List.
        :param addr: tuple
        """
        try:
            selected_clients = list(filter(lambda client: client.getpeername() == addr, self._clients))
            return selected_clients[0] if len(selected_clients) > 0 else None
        except Exception as e:
            raise ServerException(e, self.get_client_with_addr.__name__)

    def check_client_exists(self, addr: tuple) -> SelectClientSchema | None:
        """
        Check if a client exist in DB
        """
        try:
            # TODO fix IPv4 to string conversion on pydantic model
            selected_clients = list(filter(lambda client: str(client.host == addr[0]) and client.port == addr[1], self._service.select_clients()))
            return selected_clients[0] if len(selected_clients) > 0 else None
        except Exception as e:
            raise ServerException(e, self.check_client_exists.__name__)

    def get_client_info(self, client_id: int) -> SelectClientSchema | None:
        """
        Get client from DB
        """
        try:
            return self._service.select_client(client_id)
        except Exception as e:
            raise ServerException(e, self.get_client_info.__name__)

    def get_clients_info(self) -> List[dict]:
        """
        Get all clients from DB
        """
        try:
            return list(map(lambda client: client.dict(), self._service.select_clients()))
        except Exception as e:
            raise ServerException(e, self.get_clients_info.__name__)

    def get_messages_info(self) -> List[dict]:
        """
        Get all messages from DB
        """
        try:
            return list(map(lambda message: message.model_dump(), self._service.select_messages()))
        except Exception as e:
            raise ServerException(e, self.get_clients_info.__name__)

    def send_message(self, message_id: int) -> None:
        """
        Send message to client from DB
        """
        try:
            message: dict = self._service.select_message(message_id).model_dump()
            if not message:
                raise ServerException(f"Message {message_id} is not found", self.send_message.__name__)
            client_id = message["client_id"]
            # if client_id negative send message to all client
            if client_id < 0:
                for client_socket in self._clients:
                    client_socket.send(json.dumps(message['payload']).encode('utf-8'))
                return

            client: dict = self._service.select_client(client_id).model_dump()
            client_socket = self.get_client_with_addr((str(client["host"]), client["port"]))
            if not client_socket:
                raise ServerException(f"Client {client['name']} is not running", self.send_message.__name__)
            client_socket.send(json.dumps(message['payload']).encode('utf-8'))

        except Exception as e:
            raise ServerException(e, self.send_message.__name__)

    def create_client(self, name: str, host: str, port: int) -> None:
        """
        Create new in DB
        """
        try:
            client_list = self._service.select_clients()
            if len(list(filter(lambda client: client.host == host and client.port == port, client_list))):
                raise ServerException("This address is already use client", self.create_client.__name__)
            self._service.add_client(CreateOrUpdateClientSchema(name=name, host=host, port=port))
        except Exception as e:
            raise ServerException(e, self.create_client.__name__)

    def __repr__(self):
        return f'<{self.__class__.__name__}({self._host}, {self._port}): {self._clients}>'


class ServerException(Exception):
    def __init__(self, message, function):
        super().__init__(f"Error on {function}\n{message}")
