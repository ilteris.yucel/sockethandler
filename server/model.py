import contextlib
from typing import List

from sqlalchemy import URL, create_engine, Integer, String, JSON, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship, sessionmaker, Session

from base_model import Base
from settings import ServerDBSettings

server_db_settings = ServerDBSettings()

server_url = URL.create(
    drivername="mysql+mysqlconnector",
    host=server_db_settings.host,
    port=server_db_settings.port,
    database=server_db_settings.database,
    username=server_db_settings.db_user,
    password=server_db_settings.password
)

server_engine = create_engine(server_url)
server_connection = server_engine.connect()


class Client(Base):
    __tablename__ = "clients"

    id: Mapped[Integer] = mapped_column(Integer, primary_key=True)
    name: Mapped[String] = mapped_column(String(256), unique=True)
    host: Mapped[String] = mapped_column(String(256))
    port: Mapped[Integer] = mapped_column(Integer)
    messages: Mapped[List["Message"]] = relationship(back_populates="client")


class Personnel(Base):
    __tablename__ = "personnel"
    __table_args__ = {'extend_existing': True}

    id: Mapped[Integer] = mapped_column(Integer, primary_key=True)
    name: Mapped[String] = mapped_column(String(256), unique=True)
    surname: Mapped[String] = mapped_column(String(256))
    ssn: Mapped[Integer] = mapped_column(String(256))


class Message(Base):
    __tablename__ = "messages"

    id: Mapped[Integer] = mapped_column(Integer, primary_key=True)
    client_id: Mapped[Integer] = mapped_column(ForeignKey("clients.id"))
    client: Mapped["Client"] = relationship(back_populates="messages")
    payload: Mapped[JSON] = mapped_column(JSON)


server_session = sessionmaker(autocommit=False, bind=server_engine)


@contextlib.contextmanager
def get_server_session() -> Session:
    session = server_session()
    try:
        yield session
    except Exception:
        session.rollback()
    finally:
        session.close()


Base.metadata.create_all(server_engine)
