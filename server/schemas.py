from ipaddress import IPv4Address
from typing import List

from pydantic import BaseModel, field_serializer, ConfigDict

base_config = ConfigDict(from_attributes=True,
                         use_enum_values=True)


class CreateOrUpdatePersonnelSchema(BaseModel):
    model_config = base_config

    name: str
    surname: str
    ssn: str


class SelectPersonnelSchema(BaseModel):
    model_config = base_config

    id: int
    name: str
    surname: str
    ssn: str


class CreateOrUpdateClientSchema(BaseModel):
    model_config = base_config

    name: str
    host: IPv4Address
    port: int

    @field_serializer("host")
    def serialize_host(self, value: IPv4Address) -> str:
        return str(value)


class SelectClientSchema(BaseModel):
    model_config = base_config

    id: int
    name: str
    host: IPv4Address
    port: int

    @field_serializer("host")
    def serialize_host(self, value: IPv4Address) -> str:
        return str(value)


class CreateOrUpdateMessageSchema(BaseModel):
    model_config = base_config
    client_id: int
    payload: dict


class SelectMessageSchema(BaseModel):
    model_config = base_config

    id: int
    client_id: int
    payload: dict
