from typing import List

from sqlalchemy import update, select, delete, insert

from server.model import server_session
from server.model import Client, Personnel, Message
from server.schemas import CreateOrUpdatePersonnelSchema, CreateOrUpdateClientSchema, SelectClientSchema, \
    SelectPersonnelSchema, SelectMessageSchema, CreateOrUpdateMessageSchema


class ServerService:
    """
    Server service layer, handle application logic.
    It makes CRUD operation on Client, Personnel, Message table.
    """
    def __init__(self):
        # TODO dependency injection
        self._session = server_session()

    def select_client(self, client_id: int) -> SelectClientSchema:
        """
        Select client with client_id
        :param client_id:
        """
        try:
            stmt = (select(Client).where(Client.id == client_id))
            client = self._session.scalars(stmt).first()
            return SelectClientSchema.model_validate(client.__dict__)
        except Exception as e:
            raise ServerServiceException(e, self.select_client.__name__)

    def select_clients(self) -> List[SelectClientSchema]:
        """
        Select all clients
        """
        try:
            stmt = (select(Client))
            clients = self._session.scalars(stmt).all()
            return [SelectClientSchema.model_validate(client.__dict__) for client in clients if client.id != -1]
        except Exception as e:
            raise ServerServiceException(e, self.select_client.__name__)

    def add_client(self, client: CreateOrUpdateClientSchema) -> None:
        """
        Add client to DB
        :param client: CreateOrUpdateClientSchema
        """
        try:
            stmt = (insert(Client).values(**client.model_dump()))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ServerServiceException(e, self.add_client.__name__)

    def update_client(self, client_id: int, client: CreateOrUpdateClientSchema) -> None:
        """
        Update client
        :param client_id: int
        :param client: CreateOrUpdateClientSchema
        """
        try:
            stmt = (update(Client).where(Client.id == client_id).values(**client.model_dump()))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ServerServiceException(e, self.update_client.__name__)

    def delete_client(self, client_id: int) -> None:
        """
        Delete client
        :param client_id: int
        """
        try:
            stmt = (delete(Client).where(Client.id == client_id))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ServerServiceException(e, self.delete_client.__name__)

    def select_personnel(self, personnel_id: int) -> SelectPersonnelSchema:
        """
        Select personnel
        :param personnel_id: int
        """
        try:
            stmt = (select(Personnel).where(Personnel.id == personnel_id))
            personnel = self._session.scalars(stmt).first()
            return SelectPersonnelSchema.model_validate(personnel.__dict__)
        except Exception as e:
            raise ServerServiceException(e, self.select_personnel.__name__)

    def select_personals(self) -> List[SelectPersonnelSchema]:
        """
        Select all clients
        """
        try:
            stmt = (select(Personnel))
            personals = self._session.scalars(stmt).all()
            return [SelectPersonnelSchema.model_validate(personnel.__dict__) for personnel in personals]
        except Exception as e:
            raise ServerServiceException(e, self.select_client.__name__)

    def add_personnel(self, personnel: CreateOrUpdatePersonnelSchema) -> None:
        """
        Add personnel to
        :param personnel: CreateOrUpdatePersonnelSchema
        """
        try:
            stmt = (insert(Personnel).values(**personnel.model_dump()))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ServerServiceException(e, self.add_personnel.__name__)

    def update_personnel(self, personnel_id: int, personnel: CreateOrUpdatePersonnelSchema) -> None:
        """
        Update personnel
        :param personnel_id: int
        :param personnel: CreateOrUpdatePersonnelSchema
        """
        try:
            stmt = (update(Personnel).where(Personnel.id == personnel_id).values(**personnel.model_dump()))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ServerServiceException(e, self.update_client.__name__)

    def delete_personnel(self, personnel_id: int) -> None:
        """
        Delete personnel
        :param personnel_id: int
        """
        try:
            stmt = (delete(Personnel).where(Personnel.id == personnel_id))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ServerServiceException(e, self.delete_client.__name__)

    def select_message(self, message_id: int) -> SelectMessageSchema:
        """
        Select message
        :param message_id: int
        """
        try:
            stmt = (select(Message, Client).join(Message.client).where(Message.id == message_id))
            result = self._session.execute(stmt).first()
            message = SelectMessageSchema(
                id=result[0].id,
                client_id=result[0].client_id,
                client=SelectClientSchema.model_validate(result[1].__dict__),
                payload=result[0].payload
            )
            return message
        except Exception as e:
            raise ServerServiceException(e, self.select_message.__name__)

    def select_messages(self) -> List[SelectMessageSchema]:
        """
        Select all messages
        """
        try:
            stmt = (select(Message))
            messages = self._session.scalars(stmt).all()
            print(messages)
            return [SelectMessageSchema.model_validate(message.__dict__) for message in messages]
        except Exception as e:
            raise ServerServiceException(e, self.select_client.__name__)

    def add_message(self, message: CreateOrUpdateMessageSchema) -> None:
        """
        Add message
        :param message: CreateOrUpdateMessageSchema
        """
        try:
            stmt = (insert(Message).values(**message.model_dump()))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ServerServiceException(e, self.add_message.__name__)

    def update_message(self, message_id: int, message: CreateOrUpdateMessageSchema) -> None:
        """
        Update message
        :param message_id: int
        :param message: CreateOrUpdateMessageSchema
        """
        try:
            stmt = (update(Message).where(Message.id == message_id).values(**message.model_dump()))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ServerServiceException(e, self.update_message.__name__)

    def delete_message(self, message_id: int) -> None:
        """
        Delete message
        :param message_id: int
        """
        try:
            stmt = (delete(Message).where(Message.id == message_id))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ServerServiceException(e, self.delete_message.__name__)

    def __repr__(self):
        return f"{self.__class__.__name__}()"


class ServerServiceException(Exception):
    def __init__(self, message, function):
        super().__init__(f"Error on {function}\n{message}")
