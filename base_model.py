from sqlalchemy import MetaData, DateTime, Boolean, func
from sqlalchemy.orm import Mapped, mapped_column, declarative_mixin
from sqlalchemy.ext.declarative import as_declarative


@declarative_mixin
class TimestampMixin:
    created_at: Mapped[DateTime] = mapped_column(DateTime, nullable=False, default=func.now())
    updated_at: Mapped[DateTime] = mapped_column(DateTime, nullable=False, default=func.now())
    deleted: Mapped[Boolean] = mapped_column(Boolean, nullable=True, default=False)
    deleted_at: Mapped[DateTime] = mapped_column(DateTime, nullable=True, default=None)


@as_declarative()
class Base(TimestampMixin):
    metadata = MetaData()
    # id: Mapped[Any]
    __name__: str

    def __repr__(self) -> str:
        representation: str = f"{self.__class__.__name__}("
        for k, v in self.__dict__.items():
            if k != "created_at" and k != "updated_at" and k != "deleted_at" and k != "deleted":
                representation += f"{k.upper()}: {v}, "

        representation += ")"
        return representation

    def __str__(self) -> str:
        representation: str = f"{self.__class__.__name__}("
        for k, v in self.__dict__.items():
            if k != "created_at" and k != "updated_at" and k != "deleted_at" and k != "deleted":
                representation += f"{k.upper()}: {v}, "

        representation += ")"
        return representation


@as_declarative()
class ClientBase(TimestampMixin):
    metadata = MetaData()
    # id: Mapped[Any]
    __name__: str

    def __repr__(self) -> str:
        representation: str = f"{self.__class__.__name__}("
        for k, v in self.__dict__.items():
            if k != "created_at" and k != "updated_at" and k != "deleted_at" and k != "deleted":
                representation += f"{k.upper()}: {v}, "

        representation += ")"
        return representation

    def __str__(self) -> str:
        representation: str = f"{self.__class__.__name__}("
        for k, v in self.__dict__.items():
            if k != "created_at" and k != "updated_at" and k != "deleted_at" and k != "deleted":
                representation += f"{k.upper()}: {v}, "

        representation += ")"
        return representation



