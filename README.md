# SocketHandler

## USAGE

```
sudo apt-get install python3-virtualenv # install virtualenv linux system
cd [project-dir]
virtualenv venv # create virtualenv in project directory
source venv/bin/activate # activate virtualenv
pip install -r requirements.txt #install requirements of projects
python main.py #run program
```

## Architecture

Three tier application architecture is used in the application.
[Three tier application](https://en.wikipedia.org/wiki/Multitier_architecture#Three-tier_architecture)



1. model
2. service
3. handler

### Model Layer

In the model layer, a database connection is created with SQLAlchemy models.

### Service Layer

The service layer bridges the gap between SQLAlchemy models and socket.

### Handler Layer

Server and client layers transfer data via Python BSD socket.

### Schemas

Pydantic models validates data types.


### Notes

Operation Class interaction point between user and handler.

To DB for socket server create MYSQL DB on AWS RDS.

To DB for each client create SQLite DB on local.


## CLI

The command line application can still be used to intervene in the server database and create data.

```
python cli.py --help
python cli.py define-client --help
python cli.py define-message --help
python cli.py define-personnel --help
```