import os

from dotenv import load_dotenv
from pydantic import Field
from pydantic_settings import BaseSettings

load_dotenv()


class ServerDBSettings(BaseSettings):
    host: str = Field(os.environ.get("SERVER_DB_HOST", "localhost"))
    port: int = Field(os.environ.get("SERVER_DB_PORT", 3006))
    db_user: str = Field(os.environ.get("SERVER_DB_USER", "admin"))
    database: str = Field(os.environ.get("SERVER_DB_NAME", "test"))
    password: str = Field(os.environ.get("SERVER_DB_PASSWORD", "test"))

