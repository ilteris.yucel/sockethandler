import contextlib
import os

from sqlalchemy import create_engine, URL, Integer, String
from sqlalchemy.orm import Mapped, mapped_column, Session, sessionmaker

from base_model import ClientBase

parent_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))


class ClientPersonnel(ClientBase):
    __tablename__ = "personnel"
    __table_args__ = {'extend_existing': True}

    id: Mapped[Integer] = mapped_column(Integer, primary_key=True)
    name: Mapped[String] = mapped_column(String(256), unique=True)
    surname: Mapped[String] = mapped_column(String(256))
    ssn: Mapped[Integer] = mapped_column(String(256))


class ClientDBManager:
    """
    Client DB Manager create db for each client and resource session
    for ClientService
    """
    def __init__(self, db_name: str) -> None:
        self._db_name = db_name
        self._engine = create_engine(URL.create(drivername="sqlite", database=os.path.join(parent_path, f"{self._db_name}.db")))
        self._session = sessionmaker(bind=self._engine)
        ClientBase.metadata.create_all(self._engine)

    def get_client_session(self) -> Session:
        return self._session()
