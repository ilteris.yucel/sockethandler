from typing import Optional

from pydantic import BaseModel


class CreateOrUpdatePersonnelSchema(BaseModel):
    id: Optional[int]
    name: str
    surname: str
    ssn: str


class SelectPersonnelSchema(BaseModel):
    id: int
    name: str
    surname: str
    ssn: str
