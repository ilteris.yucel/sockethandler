from sqlalchemy import update, select, delete, insert

from client.model import ClientDBManager
from client.model import ClientPersonnel
from client.schemas import CreateOrUpdatePersonnelSchema, SelectPersonnelSchema


class ClientService:
    """
    Client service layer, handle application logic.
    It makes CRUD operation on Personnel table.
    """

    def __init__(self, db_name: str):
        # TODO dependency injection
        self._db_name = db_name
        self._manager = ClientDBManager(self._db_name)
        self._session = self._manager.get_client_session()

    def select_personnel(self, personnel_id: int) -> SelectPersonnelSchema:
        """
        Select personnel
        :param personnel_id:
        """
        try:
            stmt = (select(ClientPersonnel).where(ClientPersonnel.id == personnel_id))
            personnel = self._session.scalars(stmt).first()
            return SelectPersonnelSchema.model_validate(personnel.__dict__)
        except Exception as e:
            raise ClientServiceException(e, self.select_personnel.__name__)

    def add_personnel(self, personnel: CreateOrUpdatePersonnelSchema) -> None:
        """
        Add personnel
        :param personnel: CreateOrUpdatePersonnelSchema
        """
        try:
            stmt = (insert(ClientPersonnel).values(**personnel.model_dump()))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ClientServiceException(e, self.add_personnel.__name__)

    def update_personnel(self, personnel_id: int, personnel: CreateOrUpdatePersonnelSchema) -> None:
        """
        Update personnel
        :param personnel_id:
        :param personnel: CreateOrUpdatePersonnelSchema
        """
        try:
            stmt = (
                update(ClientPersonnel).where(ClientPersonnel.id == personnel_id).values(**personnel.model_dump()))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ClientServiceException(e, self.update_personnel.__name__)

    def delete_personnel(self, personnel_id: int) -> None:
        """
        Delete personnel
        :param personnel_id: id of personnel
        """
        try:
            stmt = (delete(ClientPersonnel).where(ClientPersonnel.id == personnel_id))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ClientServiceException(e, self.delete_personnel.__name__)

    def delete_all_personnel(self) -> None:
        """
        Delete all personnel
        """
        try:
            stmt = (delete(ClientPersonnel))
            self._session.execute(stmt)
            self._session.commit()
        except Exception as e:
            raise ClientServiceException(e, self.delete_personnel.__name__)


class ClientServiceException(Exception):
    def __init__(self, message, function):
        super().__init__(f"Error on {function}\n{message}")
