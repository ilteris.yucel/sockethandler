import json
import socket
import threading
import uuid
from time import sleep

from client import ClientService, CreateOrUpdatePersonnelSchema


class InovatSocketClient(socket.socket):
    """
    InovatSocketClient for socket connection.
    """
    def __init__(self, service: ClientService, family=socket.AF_INET,
                 type=socket.SOCK_STREAM, proto=0, fileno=None, **kwargs) -> None:
        super().__init__(family, type, proto, fileno)
        self._service = service
        self._port = kwargs.get('port') if 'port' in kwargs else 8000
        self._host = kwargs.get('host') if 'host' in kwargs else "127.0.0.1"
        self._name = kwargs.get('name') if 'name' in kwargs else str(uuid.uuid4())
        self._connection = None

    def bind(self, addr: tuple | None) -> None:
        """
        Bind client port to specified address.
        :param addr: tuple or None
        """
        try:
            if addr is not None:
                super().bind(addr)
                print(f'Client assign on {addr[0]} : {addr[1]}')
            else:
                super().bind((self._host, self._port))
                print(f'Client assign on {self._host} : {self._port}')
        except Exception as e:
            self.close()
            raise ClientException(e, self.bind.__name__)

    def connect(self, addr: tuple) -> None:
        """
        Connect to specified server address
        :param addr: tuple or None
        """
        try:
            super().connect(addr)
            self._connection = addr
            thread = threading.Thread(target=self.handle_connection)
            thread.start()
        except Exception as e:
            raise ClientException(e, self.connect.__name__)

    def handle_connection(self):
        """
        Get messages from socket server and handle it
        """
        try:
            print(f'Connection established on client {self._host}:{self._port}')
            while True:
                response = self.recv(1024)
                response = response.decode("utf-8")
                response = json.loads(response)
                print(f"Received: {response}")
                if response['message_type'].lower() == "close":
                    break
                if response['message_type'].lower() == "add":
                    self._service.add_personnel(CreateOrUpdatePersonnelSchema(**response['data']))
                if response['message_type'].lower() == "add_all":
                    for personnel in response['data']:
                        self._service.add_personnel(CreateOrUpdatePersonnelSchema(**personnel))
                if response['message_type'].lower() == 'delete':
                    self._service.delete_personnel(response['data']['id'])
                if response['message_type'].lower() == 'delete_all':
                    self._service.delete_all_personnel()
        except Exception as e:
            self.close()
            raise ClientException(e, self.handle_connection.__name__)
        finally:
            self.close()

    def close(self) -> None:
        """
        Close the socket client
        """
        if self._connection is not None:
            print(f'Client {self._name} disconnected from {self._connection[0]}:{self._connection[1]}')
            self._connection = None
        super().close()

    def __repr__(self):
        """
        String representation of the socket client
        """
        return f'<{self.__class__.__name__}({self._host}, {self._port}): {self._connection}>'


class ClientException(Exception):
    def __init__(self, message, function):
        super().__init__(f"Error on {function}\n{message}")
